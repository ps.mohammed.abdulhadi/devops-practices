#!/bin/bash

cd devops_sample/docker-compose/
echo "${CI_SPRING_DATASOURCE_PASS}"
echo "TAG=${newVersion}" > .env
echo "MYSQL_ROOT_PASS=${CI_SPRING_DATASOURCE_PASS}" >> .env
cat .env
docker-compose up
docker ps -a
