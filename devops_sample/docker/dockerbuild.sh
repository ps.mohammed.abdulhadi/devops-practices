#!/bin/bash


cp ./assignment-${newVersion}.jar devops_sample/docker/app.jar
cd devops_sample/docker/
echo "Artifact Version is ${newVersion}"
docker build -t appimage:${newVersion} .
echo "$CI_DOCKERHUB_PASS"
echo "$CI_DOCKERHUB_PASS" | docker login -u psmohammedabdulhadi --password-stdin
docker tag appimage:${newVersion} psmohammedabdulhadi/dockerpractice:${newVersion}
docker push psmohammedabdulhadi/dockerpractice:${newVersion}
