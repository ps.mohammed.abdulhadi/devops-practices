#!/bin/bash

docker swarm init --advertise-addr $(echo "`hostname -I | awk '{print $1}'`")
docker stack deploy -c docker-stack.yml myappstack
echo "######################################################################"
echo "    Access the App on \"http://$(echo "`hostname -I | awk '{print $1}'`"):8081/actuator/health\"  "
echo "######################################################################"
