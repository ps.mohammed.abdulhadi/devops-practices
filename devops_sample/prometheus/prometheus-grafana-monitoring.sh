#!/bin/bash
#
#
#
## deploy kube-prometheus-stack with my own values
helm install prometheus-grafana --namespace=monitoring -f ./devops_sample/prometheus/kube-prometheus-stack/myvalues.yaml ./devops_sample/prometheus/kube-prometheus-stack/. --wait --timeout=300s --force
#
## values file modifications:
## 1- line 1883: stop nodeExporter
## 2- line 885: add ingress resource for grafana to be accessed on "myapp-grafana.local"
## 3- line 3500: add serviceMonitor to configure prometheus,
##               Note: the app namespace is should be changed based on which namespace is deployed. default (testing)
## the spring boot application's grafana dashboard ID is 4701
## grafana username: admin password line 877: prom-operator
#
## deploy helm app
helm install first-release --namespace=testing ./devops_sample/helm/app/ --set mysql.auth.rootPassword=P@ssw0rd --set mysql.auth.database=assignment --set mysql.fullnameOverride=db01
