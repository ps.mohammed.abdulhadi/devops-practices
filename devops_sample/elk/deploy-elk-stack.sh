#!/bin/bash

## create log namespace
kubectl create namespace log
#
## deploying elasticsearch...
helm upgrade --install elasticsearch --namespace=log ./devops_sample/elk/elasticsearch --wait --timeout=300s --force
#
#
## deploying filebeat...
helm upgrade --install filebeat --namespace=log ./devops_sample/elk/filebeat --wait --timeout=300s --force
#
#
## deploying kibana...
helm upgrade --install kibana --namespace=log ./devops_sample/elk/kibana --wait --timeout=300s --force
#
#
## deploying spring-app...
helm install first-release --namespace=log ./devops_sample/helm/app --set mysql.auth.rootPassword=P@ssw0rd --set mysql.auth.database=assignment --set mysql.fullnameOverride=db01
