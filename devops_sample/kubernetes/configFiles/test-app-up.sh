APP_RESULT="true"
while $APP_RESULT
do
APP_STATUS=$(kubectl logs  -l app=app01 -n $CI_COMMIT_REF_SLUG | grep "Completed initialization" | cut -c 98-121)
if [ "$APP_STATUS" = "Completed initialization" ]; then
   echo "Spring app ready"
   APP_RESULT="false"
else
  echo "Spring app not ready"
  sleep 10
fi
done

echo "##################################"
echo "you may access to spring app on"
echo "https://myapp.local:443/actuator/health"
echo "##################################"
