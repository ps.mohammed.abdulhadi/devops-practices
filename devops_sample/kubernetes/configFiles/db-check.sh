#!/bin/sh

DATABASE_HOST=google.com
DATABASE_PORT=443
MAX_RETRIES=5
RETRY_INTERVAL=5
GREEN="\e[1;32m"
RED="\e[1;31m"
COLEND="\e[39m"

for i in $( seq 1 ${MAX_RETRIES} )
do
  nc -z -v -w10 ${DATABASE_HOST} ${DATABASE_PORT} 2> /dev/null
  if [ $? -eq 0 ];
  then
    echo "${GREEN}Connection succeeded!${COLEND}" && exit 0
  fi
  echo "Attempt $i: Database not up. Retrying in $RETRY_INTERVAL seconds..."
  sleep $RETRY_INTERVAL
done

echo "${RED}Database not available after $MAX_RETRIES attempts. Exiting...${COLEND}"
exit 1
